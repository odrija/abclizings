<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */
 
// Include local configuration
if (file_exists(dirname(__FILE__) . '/local-config.php')) {
	include(dirname(__FILE__) . '/local-config.php');
}

// Global DB config
if (!defined('DB_NAME')) {
	define('DB_NAME', 'wp_abclizings');
}
if (!defined('DB_USER')) {
	define('DB_USER', 'root');
}
if (!defined('DB_PASSWORD')) {
	define('DB_PASSWORD', '');
}
if (!defined('DB_HOST')) {
	define('DB_HOST', 'localhost');
}

/** Database Charset to use in creating database tables. */
if (!defined('DB_CHARSET')) {
	define('DB_CHARSET', 'utf8');
}

/** The Database Collate type. Don't change this if in doubt. */
if (!defined('DB_COLLATE')) {
	define('DB_COLLATE', '');
}

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'yOW-uZ>[7lI|mX<Qyk[0 -,3)UEq({oorQPHRS[IC+jYpgx)tvg:OmI9a}g(+.Fm');
define('SECURE_AUTH_KEY',  'r^a_1!96@aZA^gde|ohfdo2He[j^Z#8B$d,YYT#h6v+R.^_|#p$|4/4<c*:fT&tX');
define('LOGGED_IN_KEY',    '-)|U+{s~+fU!-2(gv Ee.qp49M|H|DxgO/cWrp4k#1LkajuHgjLUY0Ctl%+E-mDF');
define('NONCE_KEY',        'XGl|[q5b8Ul58p_vt;N$}Quzd5ZFxCx5+3&1U}/N<Ccp+.WJSc(bM{IX&<9o0s@e');
define('AUTH_SALT',        '|eve@lELP[Hv.rDm53+V[>p+;jtO&!PzR>_FArisf2fgTW}xSWStH5dej=ti%!-8');
define('SECURE_AUTH_SALT', 'O3*m/0DB@KaS=qWt+py1B~S_zPtX/emU*Bj|}M3hznlA<hoj *,}RdL$&u|v+gW?');
define('LOGGED_IN_SALT',   '1+lI$dNWf6AIIW.14P4{,JHs4SKUCe)58W~g8|ox2|1pbjBu[vx=f#bkmc]K*msA');
define('NONCE_SALT',       '6Dfe5?+3rOHRDRu{1j6;Or3>sy^4QP@s7mq&~GY*)=gzf2a68P[UhF#Wr$RbWy+A');


/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'abc_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');


/**
 * Set custom paths
 *
 * These are required because wordpress is installed in a subdirectory.
 */
if (!defined('WP_SITEURL')) {
	define('WP_SITEURL', 'http://' . $_SERVER['SERVER_NAME'] . '/wordpress');
}
if (!defined('WP_HOME')) {
	define('WP_HOME',    'http://' . $_SERVER['SERVER_NAME'] . '');
}
if (!defined('WP_CONTENT_DIR')) {
	define('WP_CONTENT_DIR', dirname(__FILE__) . '/content');
}
if (!defined('WP_CONTENT_URL')) {
	define('WP_CONTENT_URL', 'http://' . $_SERVER['SERVER_NAME'] . '/content');
}


/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
if (!defined('WP_DEBUG')) {
	define('WP_DEBUG', false);
}

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
