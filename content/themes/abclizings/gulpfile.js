'use strict';
// Generated on 2016-08-09 using generator-playbook 2.0.1

var gulp = require('gulp'),
    $ = require('gulp-load-plugins')(),
    browserSync = require('browser-sync'),
    runSequence = require('run-sequence'),
    del = require('del'),
    neat = require('node-neat').includePaths;

var production = false,
    paths = {
      php:     ['*.php', 'templates/**/*.php', 'includes/**/*.php'],
      styles:  'src/styles/**/*.scss',
      scripts: 'src/scripts/**/*.js',
      images:  'src/images/**/*.{png,gif,jpg,jpeg,svg}',
      vendor:  'vendor'
    };

gulp.task('styles', function () {
  return $.rubySass('src/styles/', {
      style: 'nested',
      loadPath: [paths.styles, paths.vendor].concat(neat),
      onError: console.error.bind(console, 'Sass error:')
    })
    .pipe($.autoprefixer('last 2 version', 'safari 5', 'ie 9', 'opera 12.1', 'ios 6', 'android 4'))
    .pipe(gulp.dest('.tmp/styles'))
    .pipe(browserSync.reload({ stream: true }));
});

gulp.task('scripts', function () {
  return gulp.src(paths.scripts)
    .pipe($.jshint('.jshintrc'))
    .pipe($.jshint.reporter('default'))
    .pipe(gulp.dest('.tmp/scripts'))
    .pipe(browserSync.reload({ stream: true }));
});

gulp.task('images', function () {
  return gulp.src(paths.images)
    .pipe($.imagemin({
      progressive: true,
      svgoPlugins: [{ removeViewBox: false }],
      use: []
    }))
    .pipe(gulp.dest('assets/images'));
});

gulp.task('clean', function (cb) {
  del(['.tmp', 'assets/styles/', 'assets/scripts/', 'assets/images/'], cb);
});

gulp.task('optimize', ['styles', 'scripts', 'images'], function () {
  var assets = $.useref.assets({ searchPath: ['.tmp/', 'assets', '.'] });

  return gulp.src('.tmp/**/*')
    .pipe($.if('*.css', $.minifyCss()))
    .pipe($.if('*.js', $.uglify({ mangle: {except: ['jQuery', '$']}
    }).on('error', function(e){
      console.log(e);
    })))
    .pipe($.useref())
    .pipe(gulp.dest('assets'));
});

gulp.task('php', function () {
  return gulp.src(paths.php)
    .pipe(browserSync.reload({ stream: true }));
});

gulp.task('watch', function () {
  gulp.watch(paths.styles,  ['styles', 'optimize']);
  gulp.watch(paths.scripts, ['scripts', 'optimize']);
  gulp.watch(paths.images,  ['images']);
  gulp.watch(paths.php, ['php']);
});

gulp.task('browser-sync', ['styles', 'scripts'], function () {
  browserSync({
    notify: false,
    port: 9000,
    proxy: 'abclizings.test'
  });
});

gulp.task('serve', function () {
  runSequence('clean', 'optimize', ['browser-sync', 'watch']);
});

gulp.task('default', function () {
  production = true;
  runSequence('clean', 'build');
});
