<?php

/**
 * Helper constants
 */

define( 'ABC_THEME', __FILE__ );
define( 'ABC_THEME_DIR',
untrailingslashit( dirname( ABC_THEME ) ) );
define( 'ABC_THEME_URL', get_bloginfo( 'template_url' ) );

require_once ABC_THEME_DIR . '/functions/00-cleanup.php';
require_once ABC_THEME_DIR . '/functions/01-enqueue.php';
require_once ABC_THEME_DIR . '/functions/02-cpt.php';
require_once ABC_THEME_DIR . '/functions/03-acf.php';
require_once ABC_THEME_DIR . '/functions/04-init.php';
require_once ABC_THEME_DIR . '/functions/05-classes.php';
require_once ABC_THEME_DIR . '/functions/06-theme-helpers.php';
require_once ABC_THEME_DIR . '/functions/07-translations.php';
require_once ABC_THEME_DIR . '/functions/08-admin.php';

require_once ABC_THEME_DIR . '/functions/theme/ajax-filter-sort.php';
