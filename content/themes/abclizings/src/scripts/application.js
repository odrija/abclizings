(function($) {
    'use strict';

    var Menu = function(settings) {
        var self = {
            config: {
                openTrigger: '#js-menu-trigger',
                closeTrigger: '#js-menu-close-trigger',
                menuClass: 'menu--mobile'
            },

            init: function() {
                if (settings && typeof(settings) == 'object') {
                    $.extend(self.config, settings);
                }

                self.cacheDOM();
                self.$openTrigger.on('click', function(e) {
                    e.preventDefault();
                    self.$menu.fadeIn();
                    self.scrollToggle();
                });
                self.$closeTrigger.on('click', function(e) {
                    e.preventDefault();
                    self.$menu.fadeOut();
                    self.scrollToggle();
                });
                self.$scrollTriggers.on('click', function(e) {
                    e.preventDefault();
                    self.scrollToSection(this);
                });
            },

            cacheDOM: function() {
                self.$openTrigger    = $(self.config.openTrigger);
                self.$closeTrigger   = $(self.config.closeTrigger);
                self.$menu           = $('.' + self.config.menuClass);
                self.$body           = $('body');
                self.$scrollTriggers = $('a[href^="#"]');
            },

            scrollToggle: function(enable) {
                if (enable) {
                    self.$body.removeClass('noscroll');
                    self.$body.unbind('touchmove');
                } else {
                    self.$body.toggleClass('noscroll');
                    if ( self.$body.hasClass('noscroll') ) {
                        self.$body.bind('touchmove', function(e) {
                            e.preventDefault();
                        });
                    } else {
                        self.$body.unbind('touchmove');
                    }
                }
            },

            scrollToSection: function(el) {
                var target = $(el).attr('href');

                if ($(target).length > 0) {
                    $('html, body').animate({
                        scrollTop: $(target).offset().top - 20
                    }, 'medium', function() {
                        self.$menu.fadeOut();
                        self.scrollToggle(true);
                    });
                } else {
                    window.location.href = '//' + location.host + '/' + target;
                }
            }
                
        };
        
        return {
            init: self.init
        };
    };

    var lightbox = function() {
        var settings = {
            gallery: true,
            item: 1,
            adaptiveHeight: true,
            thumbItem: 7,
            thumbMargin: 0,
            slideMargin: 0,
            galleryMargin: 0,
            addClass: 'single-car__gallery',
            responsive : [
                {
                    breakpoint: 520,
                    settings: {
                        thumbItem: 3
                    }
                }
            ]
        };

        if ($('#js-car-gallery').length > 0) {
            var car_gallery = $('#js-car-gallery').lightSlider(settings);
        }
    };

    var filter = function() {
        $('select').niceSelect();
    };

    var checkbox = {
        init: function() {
            /* This code works when not using CF7 */
            // var $checkboxes = $('.custom-checkbox');
            // var $labels = $('.custom-checkbox').next('label');
    
            // $checkboxes.each(function() {
            //     $(this).click(function() {
            //         $(this).toggleClass('checked');
            //     });
            // });
    
            // $labels.each(function() {
            //     $(this).on('tap click', 'a', function(event) {
            //         event.stopPropagation();
            //         event.preventDefault();
            //         window.open($(this).attr('href'), $(this).attr('target'));
            //         return false;
            //     });
            // });

            /* This code must be used with CF7 */
            var $checkboxes = $('.custom-checkbox');
            var $labels = $('.custom-checkbox').parents('.checkbox-wrapper').find('label');
    
            $checkboxes.each(function() {
                $(this).find('input').prop('checked', false);
                $(this).click(function() {
                    $(this).toggleClass('checked');
                    $(this).find('input').prop('checked', !$(this).prop('checked'));
                    $(this).prop('checked', !$(this).prop('checked'));
                });
            });
    
            $labels.each(function() {
                $(this).click(function() {
                    var id = $(this).attr('for');
                    var $checkbox = $('input[name="' + id + '[]"]');
                    $('#' + id).toggleClass('checked');
                    $checkbox.prop('checked', !$checkbox.prop('checked'));
                });
            });
    
            document.addEventListener( 'wpcf7mailsent', function( event ) {
                $checkboxes.each(function() {
                    $(this).find('input').prop('checked', false);
                    $(this).removeClass('checked');
                });
            }, false );
        }
    };

    var enquiry = {
        init: function() {
            this.getCarURL();
            this.setCarURL();
        },

        getCarURL: function() {
            this.carURL = ( typeof php_vars !== 'undefined' && php_vars.car_url ) ? php_vars.car_url : false;
        },

        setCarURL: function() {
            if (this.carURL) {
                $('#link').val(this.carURL);
            }
        }
    };

    var init = function() {
        var menu = new Menu();
        menu.init();
        lightbox();
        filter();
        checkbox.init();
        enquiry.init();

        var myLazyLoad = new LazyLoad({
            elements_selector: '.lazy'
        });
    };

    $(document).ready(function() {
        init();
    });

    $(document).click(function(e) {
        if ($('.wpcf7-mail-sent-ok').length > 0 && e.target != $('.wpcf7-mail-sent-ok')) {
            $('.wpcf7-mail-sent-ok').fadeOut('fast');
        }
    });
    
})(jQuery);