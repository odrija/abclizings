(function($) {
    'use strict';

    var sortCars = function() {
        var self = {
            init: function() {
                self.cacheDOM();
                self.$sortForm.on('change', function() {
                    self.doSorting();
                });
            },

            cacheDOM: function() {
                self.$sortForm    = $('#js-car-sort');
                self.ajaxUrl      = self.$sortForm.attr('action');
                self.type         = self.$sortForm.attr('method');
                self.$container   = $('.inventory__wrapper');
                self.$input       = $('#filter-status');
                self.$filterForm  = $('#js-car-filter');
            },

            doSorting: function() {
                var filter_query = false;
                if (self.$input.val() == 'true') {
                    filter_query = self.$filterForm.serialize();
                }

                $.ajax({
                    url: self.ajaxUrl,
                    data: {
                        'action': 'sort_cars',
                        'sort_type': self.$sortForm.serialize(),
                        'filter_query': filter_query
                    },
                    type: self.type,
                    beforeSend: function(xhr) {
                        self.$container.html('<span class="message">Šķiro...</span>');
                    },
                    error: function(data) {
                        // console.log(data);
                        self.$container.html('<span class="message">Notikusi kļūda!</span>');
                    },
                    success: function(data) {
                        // console.log(data);
                        self.$container.html(data.output);
                    }
                });
                return false;
            }
        };

        return {
            init: self.init
        };
    };

    var filterCars = function() {
        var self = {
            init: function() {
                self.cacheDOM();
                self.toggleStatus();
                self.$filterForm.on('submit', function(e) {
                    e.preventDefault();
                    self.doFiltering();
                });
                self.$filterForm.on('change', function() {
                    self.toggleStatus();
                });
            },

            cacheDOM: function() {
                self.$filterForm  = $('#js-car-filter');
                self.ajaxUrl    = self.$filterForm.attr('action');
                self.type       = self.$filterForm.attr('method');
                self.$container = $('.inventory__wrapper');
                self.$input     = $('#filter-status');
            },

            toggleStatus: function() {
                var status  = 'false',
                    $inputs = self.$filterForm.find('select');

                $inputs.each(function() {
                    if ($(this).val() != 'all') {
                        status = 'true';
                    }
                });
                
                self.$input.val(status);
            },

            doFiltering: function() {
                $.ajax({
                    url: self.ajaxUrl,
                    data: {
                        'action': 'filter_cars',
                        'filter': self.$filterForm.serialize()
                    },
                    type: self.type,
                    beforeSend: function(xhr) {
                        self.$container.html('<span class="message">Atlasa rezultātus...</span>');
                    },
                    error: function(data) {
                        // console.log(data);
                        self.$container.html('<span class="message">Notikusi kļūda!</span>');
                    },
                    success: function(data) {
                        // console.log(data);
                        self.$container.html(data.output);
                    }
                });
                return false;
            }
        };

        return {
            init: self.init
        };
    };

    $(document).ready(function() {
        var sorting = new sortCars();
        sorting.init();

        var filtering = new filterCars();
        filtering.init();
    });
})(jQuery);
