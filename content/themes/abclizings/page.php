<?php

/**
 * Hello, friend!
 */

get_header(); ?>

<div class="container">
    <h1><?php the_title(); ?></h1>

    <div class="block-group">
        <div class="block-5">
            <?php the_content(); ?>
        </div>
        <div class="block-3"></div>
    </div>
</div>

<?php get_footer();

