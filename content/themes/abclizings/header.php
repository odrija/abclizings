<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7" <?php language_attributes() ?>><![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8" <?php language_attributes() ?>><![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9" <?php language_attributes() ?>><![endif]-->
<!--[if gt IE 8]><!-->
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<title><?php bloginfo( 'name' ); ?> | <?php is_front_page() ? bloginfo( 'description' ) : wp_title( '' ); ?></title>

	<!-- Favicons -->
	<link rel="apple-touch-icon" sizes="180x180" href="<?php echo ABC_THEME_URL; ?>/assets/images/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="96x96" href="<?php echo ABC_THEME_URL; ?>/assets/images/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="32x32" href="<?php echo ABC_THEME_URL; ?>/assets/images/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="<?php echo ABC_THEME_URL; ?>/assets/images/favicon-16x16.png">

	<!-- Facebook OG -->
	<meta property="og:url"                content="<?php echo get_permalink(); ?>" />
	<meta property="og:type"               content="website" />
	<meta property="og:title"              content="<?php echo bloginfo( 'name' ); ?>" />
	<meta property="og:description"        content="<?php is_front_page() ? bloginfo( 'description' ) : wp_title( '' ); ?>" />
	<meta property="og:image"              content="<?php echo ABC_THEME_URL; ?>/screenshot.png" />

	<!-- Twitter OG -->
	<meta name="twitter:card" content="summary" />

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

	<header class="header <?php echo ( is_front_page() ) ? ' header--overlay' : ''; ?>">
		<div class="container">
			<a href="<?php echo home_url(); ?>" class="logo">
				<svg viewBox="0 0 65 80" xmlns="http://www.w3.org/2000/svg" fill-rule="evenodd" clip-rule="evenodd" stroke-linejoin="round" stroke-miterlimit="1.414"><g fill="#fff"><g fill-rule="nonzero"><path d="M4.651 69.921h2.583v7.693h4.193v2.202H4.651v-9.895zM12.599 69.921h2.583v9.895h-2.583v-9.895zm-.89-2.301h4.362v1.482h-4.362V67.62zM16.706 69.921h8.498v1.694l-5.223 6.126h5.308v2.075h-8.766v-1.679l5.223-6.141h-5.04v-2.075zM26.644 69.921h2.583v9.895h-2.583zM37.909 69.921h2.4v9.895H38.05l-4.489-5.914v5.914h-2.414v-9.895h2.273l4.503 5.957-.014-5.957zM48.792 74.763h2.231v3.938c-.527.386-1.174.699-1.941.939-.767.24-1.494.36-2.181.36-.998 0-1.899-.224-2.703-.671a4.958 4.958 0 0 1-1.899-1.842c-.461-.781-.692-1.656-.692-2.625 0-.96.238-1.828.713-2.605a5.013 5.013 0 0 1 1.962-1.828c.833-.442 1.763-.663 2.788-.663.734 0 1.468.134 2.202.402s1.356.628 1.864 1.08l-1.468 1.807a4.136 4.136 0 0 0-1.235-.805 3.52 3.52 0 0 0-1.377-.296 2.72 2.72 0 0 0-1.426.381 2.743 2.743 0 0 0-1.002 1.045 3.015 3.015 0 0 0-.367 1.482c0 .555.123 1.056.367 1.503a2.7 2.7 0 0 0 1.017 1.052c.432.254.912.381 1.439.381.499 0 1.069-.16 1.708-.48v-2.555zM59.549 72.9a8.667 8.667 0 0 0-1.687-.763c-.597-.197-1.099-.296-1.503-.296-.32 0-.572.059-.755.176a.575.575 0 0 0-.276.516c0 .216.08.397.24.543.16.146.358.264.593.353.235.089.584.205 1.045.346.687.197 1.254.395 1.701.593.447.197.83.501 1.15.91.32.41.48.944.48 1.602 0 .659-.174 1.221-.522 1.687-.348.466-.826.817-1.433 1.052-.607.235-1.282.353-2.026.353a7.326 7.326 0 0 1-2.449-.431 6.717 6.717 0 0 1-2.11-1.178l1.002-2.019a6.233 6.233 0 0 0 1.772 1.087c.682.282 1.287.423 1.814.423.385 0 .689-.07.91-.211a.694.694 0 0 0 .332-.622.735.735 0 0 0-.247-.564 1.808 1.808 0 0 0-.621-.36c-.25-.09-.6-.195-1.052-.318a13.062 13.062 0 0 1-1.68-.571 2.97 2.97 0 0 1-1.136-.876c-.315-.39-.473-.91-.473-1.559 0-.612.162-1.149.487-1.61.325-.461.786-.816 1.383-1.065.598-.25 1.292-.375 2.082-.375.716 0 1.421.102 2.118.304a6.957 6.957 0 0 1 1.835.812l-.974 2.061z"/></g><path d="M32.347 0c8.9 0 16.509 3.158 22.825 9.473 6.316 6.316 9.474 13.94 9.474 22.871 0 8.902-3.158 16.512-9.474 22.828-6.316 6.315-13.925 9.474-22.825 9.474-8.934 0-16.557-3.159-22.873-9.474C3.158 48.856 0 41.246 0 32.344c0-8.931 3.158-16.555 9.474-22.871C15.79 3.158 23.413 0 32.347 0zm8.761 25.903c-.178.92-.899 2.284-1.938 3.561-1.07 1.308-1.691 2.256-1.871 2.849.089.772.67 1.752 1.737 2.939 1.128 1.246 1.894 2.256 2.072 3.028v21.669c-2.435.981-5.381 1.284-8.619 1.314-3.236.029-6.405-.358-8.933-1.307V4.63c5.703-1.931 11.812-1.831 17.552-.159.089 1.219 0 21.432 0 21.432zm-25.992 9.781v19.993s1.455 1.157 5.542 3.239V5.565S3.338 13.013 3.372 32.389c0 0-.839 11.141 8.643 20.623l-.012-9.12s-2.568-2.905-2.345-8.208h5.458zm38.757 5.976c-.005.015-1.594 5.156-4.16 5.928V17.056c2.569.774 4.16 5.929 4.16 5.929h6.078C55.513 9.819 44.115 5.796 44.115 5.796v53.053s11.398-4.023 15.836-17.189h-6.078zm-20.985-6.583c2.314 0 2.443 2.889 2.443 2.889v17.137c-3.391 1.29-5.859 0-5.859 0V35.077h3.416zM15.14 17.42c-5.159 4.814-5.705 12.173-5.705 12.173h5.705V17.42zm14.332 12.133h3.416c2.314 0 2.443-2.888 2.443-2.888V9.528c-3.391-1.29-5.859 0-5.859 0v20.025z"/></g></svg>
			</a>

			<a href="javascript:void(0)" class="menu-trigger" id="js-menu-trigger">
				<span><?php echo _t( 'Izvēlne' ); ?></span>
					<svg viewBox="0 0 23 18" xmlns="http://www.w3.org/2000/svg" fill-rule="evenodd" clip-rule="evenodd" stroke-linejoin="round" stroke-miterlimit="1.414"><path fill="#fff" d="M3.481 16h19.146v1.5H3.481zM5.222 8h17.406v1.5H5.222zM0 0h22.627v1.5H0z"/></svg>
			</a>

			<?php
			$facebook  = ( get_field( 'contact_facebook', 'options' ) ) ? get_field( 'contact_facebook', 'options' ) : false;
			$instagram = ( get_field( 'contact_instagram', 'options' ) ) ? get_field( 'contact_instagram', 'options' ) : false;
			?>

			<nav class="menu--mobile">
				<div class="menu--mobile__wrapper">
					<a href="javascript:void(0)" class="menu-close-trigger" id="js-menu-close-trigger">
						<svg viewBox="0 0 19 19" xmlns="http://www.w3.org/2000/svg" fill-rule="evenodd" clip-rule="evenodd" stroke-linejoin="round" stroke-miterlimit="1.414"><g fill="#1f2a52"><path d="M16.971 18.096L0 1.125 1.125 0l16.971 16.971-1.125 1.125z"/><path d="M18.096 1.125L1.125 18.096 0 16.971 16.971 0l1.125 1.125z"/></g></svg>
					</a>

					<?php
					wp_nav_menu( array(
						'theme_location' => 'header-menu',
						'items_wrap'     => '%3$s',
						'container'      => false,
						'walker'         => new Walker_Main_Menu(),
					) );
					?>

					<a href="<?php echo home_url(); ?>/kontakti" class="menu__item"><?php echo _t( 'Kontakti' ); ?></a>

					<?php if ( $facebook || $instagram ) : ?>
					<div class="menu__item">
						<?php if ( $facebook ) : ?>
						<a href="<?php echo $facebook; ?>" class="menu__social">
							<i class="icon ion-logo-facebook"></i>
						</a>
						<?php endif; ?>
						<?php if ( $instagram ) : ?>
						<a href="<?php echo $instagram; ?>" class="menu__social">
							<i class="icon ion-logo-instagram"></i>
						</a>
						<?php endif; ?>
					</div>
					<?php endif; ?>

				</div>
			</nav>

			<nav class="menu">
				<?php
				wp_nav_menu( array(
					'theme_location' => 'header-menu',
					'items_wrap'     => '<div class="menu__left">%3$s</div>',
					'container'      => false,
					'walker'         => new Walker_Main_Menu(),
				) );
				?>

				<div class="menu__right">
					<a href="<?php echo home_url(); ?>/kontakti" class="menu__item"><?php echo _t( 'Kontakti' ); ?></a>

					<?php if ( $facebook ) : ?>
					<a href="<?php echo $facebook; ?>" target="_blank" title="ABC Līzings Facebook" class="menu__social">
						<i class="icon ion-logo-facebook"></i>
					</a>
					<?php endif; ?>

					<?php if ( $instagram ) : ?>
					<a href="<?php echo $instagram; ?>" target="_blank" title="ABC Līzings Instagram" class="menu__social">
						<i class="icon ion-logo-instagram"></i>
					</a>
					<?php endif; ?>
				</div>
			</nav>
		</div>
	</header>
