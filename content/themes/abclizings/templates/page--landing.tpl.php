<?php
/**
 * Template Name: Landing page
 */

get_header(); ?>

<?php
// Lapas augšdaļa.
$heading      = get_field( 'landing_page_heading' );
$heading_link = get_field( 'landing_page_button' );

// Par mums.
$about_blocks = get_field( 'landing_about_blocks' );

// Pakalpojumi.
$services_blocks = get_field( 'landing_services_blocks' );

// Kontakti.
$map_url         = get_field( 'landing_contacts_map', 'option' );
$contact_info    = get_field( 'landing_contacts_info' );
$contact_address = get_field( 'landing_contacts_address', 'option' );
$workdays        = get_field( 'landing_contacts_workdays', 'option' );
$saturdays       = get_field( 'landing_contacts_saturdays', 'option' );
$sundays         = get_field( 'landing_contacts_sundays', 'option' );
?>

<section class="hero">
	<div class="container">
		<div class="hero__content">
			<h1><?php echo $heading; ?></h1>
			<a href="<?php echo $heading_link['url']; ?>" target="<?php echo $heading_link['target']; ?>"  class="hero__link">
				<span><?php echo $heading_link['title']; ?></span>
				<span class="arrow">
					<svg viewBox="0 0 10 18" xmlns="http://www.w3.org/2000/svg" fill-rule="evenodd" clip-rule="evenodd" stroke-linejoin="round" stroke-miterlimit="1.5"><path d="M1 1l7.93 7.93L1 16.859" fill="none" stroke="#d9ad61" stroke-width="2"/></svg>
				</span>
			</a>
		</div>
	</div>
</section>

<section class="about text-section">
	<div class="container">
		<h2 class="text-section__title"><?php echo _t( 'Par mums' ); ?></h2>
		<?php foreach ( $about_blocks as $block ) : ?>
			<div class="block-group">
				<div class="block-3">
					<h3><?php echo $block['landing_about_blocks_header']; ?></h3>
					<?php if ( $block['landing_about_blocks_image'] ) : ?>
					<div class="block-3 block--fill-left">
						<img src="<?php echo $block['landing_about_blocks_image']['url']; ?>" alt="<?php echo $block['landing_about_blocks_image']['alt']; ?>">
					</div>
					<?php endif; ?>
				</div>
				<div class="block-5">
					<?php echo $block['landing_about_blocks_text']; ?>
				</div>
			</div>
		<?php endforeach; ?>
	</div>
</section>

<section class="services text-section" id="services">
	<div class="container">
		<h2 class="text-section__title"><?php echo _t( 'Pakalpojumi' ); ?></h2>
		<?php foreach ( $services_blocks as $block ) : ?>
			<div class="block-group">
				<div class="block-3">
					<h3><?php echo $block['landing_services_blocks_header']; ?></h3>
				</div>
				<div class="block-5">
					<?php echo $block['landing_services_blocks_text']; ?>
				</div>
			</div>
		<?php endforeach; ?>
	</div>
</section>

<section class="contacts text-section">
	<div class="container">
		<h2 class="text-section__title"><?php echo _t( 'Kontakti' ); ?></h2>
		<div class="block-group">
			<div class="block-3 ">
				<div id="js-map" class="block--fill-left map hide-xs hide-md">
					<iframe src="<?php echo $map_url; ?>" width="100%" height="100%" style="border:none;"></iframe>
				</div>
			</div>
			<div class="block-5">
				<div class="contact__row">
					<div class="contact__block">
						<h3><?php echo _t( 'Kontakti' ); ?></h3>
						<?php echo $contact_info; ?>
					</div>
					<div class="contact__block">
						<h3><?php echo _t( 'Adrese' ); ?></h3>
						<?php echo $contact_address; ?>
					</div>
				</div>
				<div class="contact__row">
					<div class="contact__block hide-xs">
						<h3><?php echo _t( 'Darba laiks' ); ?></h3>
						<p>
							<?php echo _t( 'Pirmdiena – Piektdiena' ); ?> <br>
							<?php echo _t( 'Sestdiena' ); ?> <br>
							<?php echo _t( 'Svētdiena' ); ?>
						</p>
					</div>
					<div class="contact__block hide-xs">
						<h3>&nbsp;</h3>
						<p>
							<?php echo $workdays; ?> <br>
							<?php echo $saturdays; ?> <br>
							<?php echo $sundays; ?>
						</p>
					</div>
					<div class="show-xs">
						<div class="contact__block">
							<h3><?php echo _t( 'Darba laiks' ); ?></h3>
							<div class="row">
								<p>
									<?php echo _t( 'P – P' ); ?> <br>
									<?php echo _t( 'S' ); ?> <br>
									<?php echo _t( 'Sv' ); ?>
								</p>
								<p>
									<?php echo $workdays; ?> <br>
									<?php echo $saturdays; ?> <br>
									<?php echo $sundays; ?>
								</p>
							</div>
						</div>					
					</div>
				</div>
			</div>
		</div>
		<div class="block-group">
			<div class="map show-xs show-md" id="js-map">
				<iframe src="<?php echo $map_url; ?>" width="100%" height="100%" style="border:none;"></iframe>
			</div>
		</div>
	</div>
</section>


<?php
get_footer();
