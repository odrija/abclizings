<?php get_header(); ?>

<?php $car = new InventoryItem( get_the_ID() ); ?>

<article class="single-car">
	<div class="container">
		<div class="block-group">
			<div class="block-2">
				<?php echo $car->show_car_data(); ?>
			</div>
			<div class="block-6">
				<?php echo $car->show_title_bar(); ?>

				<?php echo $car->show_car_description(); ?>

				<?php echo $car->show_car_gallery(); ?>
			</div>
		</div>
	</div>
</article>

<?php get_footer(); ?>
