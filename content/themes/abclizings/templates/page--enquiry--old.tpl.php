<?php
get_header();
?>

<section class="enquiry">
	<div class="container">
		<form action="" id="pieteikuma-forma">
			<section class="text-section">
				<h2 class="text-section__title">Personas dati</h2>

				<div class="form-part">
					<div class="form-item">
						<label for="first-name">Vārds</label>
						<input id="first-name" name="first-name" type="text">
						<p class="message"></p>
					</div>

					<div class="form-item">
						<label for="last-name">Uzvārds</label>
						<input id="last-name" name="last-name" type="text">
						<p class="message"></p>
					</div>

					<div class="form-item">
						<label for="social-number">Personas kods</label>
						<input id="social-number" name="social-number" type="text">
						<p class="message"></p>
					</div>

					<div class="form-item">
						<label for="email">E-pasts</label>
						<input id="email" name="email" type="email">
						<p class="message"></p>
					</div>

					<div class="form-item">
						<label for="telephone">Tālruņa numurs</label>
						<input id="telephone" name="telephone" type="text">
						<p class="message"></p>
					</div>

					<div class="form-item">
						<label for="address">Dzīvesvietas adrese</label>
						<input id="address" name="address" type="text">
						<p class="message"></p>
					</div>
				</div>
			</section>

			<section class="text-section">
				<h2 class="text-section__title">Finanšu dati</h2>
				
				<div class="form-part">
					<div class="form-item">
						<label for="employer">Darba vietas nosaukums</label>
						<input id="employer" name="employer" type="text">
						<p class="message"></p>
					</div>

					<div class="form-item">
						<label for="experience">Stāžs (mēnešos)</label>
						<input id="experience" name="experience" type="number">
						<p class="message"></p>
					</div>

					<div class="form-item">
						<label for="income">Ienākumi mēnesī (neto)</label>
						<input id="income" name="income" type="number">
						<p class="message"></p>
					</div>

					<div class="form-item">
						<label for="credit">Esošo kredītu maksājumi</label>
						<input id="credit" name="credit" type="number">
						<p class="message"></p>
					</div>

					<div class="form-item">
						<label for="sum">Aizdevuma summa</label>
						<input id="sum" name="sum" type="number">
						<p class="message"></p>
					</div>

					<div class="form-item">
						<label for="term">Vēlamais termiņš (mēnešos)</label>
						<input id="term" name="term" type="number">
						<p class="message"></p>
					</div>

					<div class="form-item">
						<label for="statement">Konta izraksts (PDF)</label>
						<input id="statement" name="statement" type="file">
						<p class="message"></p>
						<p class="help">Ieņēmumu un izdevumu izdruka no internetbankas par pēdējiem 6 mēnešiem līdz šodienai</p>
					</div>
				</div>
			</section>

			<section class="text-section">
				<h2 class="text-section__title">Informācija par auto</h2>

				<div class="form-part">
					<div class="form-item">
						<label for="link">Sludinājuma saite</label>
						<input id="link" name="link" type="url">
						<p class="message"></p>
					</div>
					<div class="form-item">
						<label for="registration">Automašīnas reģ. nr. (ja ir)</label>
						<input id="registration" name="registration" type="text">
						<p class="message"></p>
					</div>
					<div class="form-item">
						<label for="model">Automašīnas modelis</label>
						<input id="model" name="model" type="text">
						<p class="message"></p>
					</div>
				</div>
			</section>

			<section class="text-section">
				<div class="form-part">
					<div class="form-item">
						<button type="submit" class="button button--submit">Pieteikties</button>
						<div class="checkbox-wrapper">
							<input type="checkbox" name="agreement" id="agreement" class="custom-checkbox">
							<label for="agreement">
								Piekrītu personīgo datu apstrādei un apstiprinu ievadīto datu patiesumu
								<a href="#" target="_blank">(Uzzināt vairāk)</a>
							</label>
						</div>
						<p class="center-text">Ja piedāvājums tevi apmierinās varēsi to parakstīt mūsu oﬁsā.</p>   
					</div>
				</div>
			</section>
		</form>
	</div>
</section>

<?php get_footer(); ?>

