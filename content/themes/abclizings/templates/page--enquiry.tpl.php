<?php
/**
 * Template Name: Enquiry Form
 */

get_header();

$subheading  = get_field( 'enquiry_subheader' );
$description = get_field( 'enquiry_description' );
$image       = get_field( 'enquiry_image' );
$form        = get_field( 'enquiry_form' );

global $wp_query;
$car_id = ( isset( $wp_query->query_vars['car_id'] ) ) ? intval( $wp_query->query_vars['car_id'] ) : false;

if ( $car_id ) {
	$car_post = get_post( $car_id );
	$car_url  = ( 'cars' === $car_post->post_type && 'publish' === $car_post->post_status ) ? get_permalink( $car_id ) : false;
	wp_localize_script( 'application-js', 'php_vars', array( 'car_url' => $car_url ) );
}

?>

<section class="enquiry">
	<div class="container">
		<div class="block-group">
			<div class="block-3">

				<h2 class="text-section__title"><?php echo $subheading; ?></h2>

				<?php if ( $description ) : ?>
					<p><?php echo $description; ?></p>
				<?php endif; ?>

				<?php if ( $image ) : ?>
				<div class="block--fill-left">
					<img src="<?php echo $image['sizes']['large']; ?>" alt="<?php echo $image['alt']; ?>">
				</div>
				<?php endif; ?>

			</div>
			<div class="block-4">
				<?php echo do_shortcode( "[contact-form-7 id='{$form->ID}']" ); ?>
				<!--
				<form action="" id="pieteikuma-forma">
					<div class="form-part">
						<div class="form-item">
							<label for="first-name">Vārds<sup>*</sup></label>
							<input id="first-name" name="first-name" type="text" required>
							<p class="message"></p>
						</div>

						<div class="form-item">
							<label for="last-name">Uzvārds<sup>*</sup></label>
							<input id="last-name" name="last-name" type="text" required>
							<p class="message"></p>
						</div>

						<div class="form-item">
							<label for="tel">Telefona nr.<sup>*</sup></label>
							<input id="tel" name="tel" type="text" required>
							<p class="message"></p>
						</div>

						<div class="form-item">
							<label for="email">E-pasts<sup>*</sup></label>
							<input id="email" name="email" type="email" required>
							<p class="message"></p>
						</div>

						<div class="form-item">
							<label for="link">Sludinājuma saite</label>
							<input id="link" name="link" type="url">
							<p class="message"></p>
						</div>
					</div>

					<div class="form-part">
						<div class="form-item">
							<button type="submit" class="button button--submit">Pieteikties</button>
							<div class="checkbox-wrapper">
								<input type="checkbox" name="agreement[]" id="agreement" class="custom-checkbox" required>
								<label for="agreement">
									Piekrītu personīgo datu apstrādei un apstiprinu ievadīto datu patiesumu
									<a href="<?php echo home_url(); ?>/privatuma-politika" target="_blank">(Uzzināt vairāk)</a>
								</label>
							</div>
						</div>
					</div>
				</form>
				-->
			</div>
			<div class="block-1"></div>
		</div>
	</div>
</section>

<?php get_footer(); ?>
