<?php
/**
 * Template Name: Contact Page
 */

get_header();

// Kontakti.
$map_url         = get_field( 'landing_contacts_map', 'option' );
$contact_address = get_field( 'landing_contacts_address', 'option' );
$workdays        = get_field( 'landing_contacts_workdays', 'option' );
$saturdays       = get_field( 'landing_contacts_saturdays', 'option' );
$sundays         = get_field( 'landing_contacts_sundays', 'option' );
$contact_blocks  = array_chunk( get_field( 'contact_blocks' ), 2 );

?>

	<section class="contact">
	<div class="container contacts">
		<div class="block-group">
			<div class="block-3">
				<div class="map block--fill-left" id="js-map">
					<iframe src="<?php echo $map_url; ?>" width="100%" height="100%" style="border:none;"></iframe>
				</div>
			</div>
			<div class="block-5">
				<div class="contact__row">
					<div class="contact__block">
						<h3><?php echo _t( 'Adrese' ); ?></h3>
						<?php echo $contact_address; ?>
					</div>
					<div class="contact__block">
						<h3><?php echo _t( 'Darba laiks' ); ?></h3>
						<div class="contact__block--half">
							<p>
								<?php echo _t( 'P – P' ); ?> <br>
								<?php echo _t( 'S' ); ?> <br>
								<?php echo _t( 'Sv' ); ?>
							</p>
						</div>
						<div class="contact__block--half">
							<p>
								<?php echo $workdays; ?> <br>
								<?php echo $saturdays; ?> <br>
								<?php echo $sundays; ?>
							</p>
						</div>
					</div>
				</div>

				<?php foreach ( $contact_blocks as $row ) : ?>
				<div class="contact__row">
					<?php foreach ( $row as $block ) : ?>
					<div class="contact__block">
						<h3><?php echo $block['contact_blocks_header']; ?></h3>
						<?php echo strip_tags( $block['contact_blocks_text'], '<p><br><strong><em><a>' ); ?>
					</div>
					<?php endforeach; ?>
				</div>		
				<?php endforeach; ?>

			</div>
		</div>
	</div>
</section>


<?php get_footer(); ?>
