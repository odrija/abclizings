<?php

/**
 * Template Name: Inventory
 */

get_header(); ?>

<?php
$heading    = get_field( 'inventory_heading' );
$subheading = get_field( 'inventory_subheading' );
?>

<?php CarFilter::init(); ?>

<section class="intro">
	<div class="container">
		<div class="block-group">
			<div class="block-7">
				<h3><?php echo $heading; ?></h3>
				<h4><?php echo $subheading; ?></h4>
			</div>
			<div class="block-1"></div>
		</div>
	</div>
</section>

<section class="inventory__filters">
	<div class="container">

		<article class="filters">
			<?php CarFilter::show_filter(); ?>
		</article>

		<article class="sorting">
			<div class="block-group">
				<div class="block-4">
					<?php CarFilter::show_cars_for_sale_count(); ?>
				</div>
				<div class="block-4">
					<?php CarFilter::show_sorting(); ?>	
				</div>
			</div>
		</article>
	</div>
</section>

<section class="inventory">
	<div class="container">
		<div class="inventory__wrapper">

			<?php CarFilter::show_all_cars(); ?>
		</div>
	</div>
</section>

<?php get_footer(); ?>
