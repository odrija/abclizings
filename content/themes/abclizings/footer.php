	<script type="application/ld+json">
	{
	"@context": "http://schema.org",
	"@type": "AutomotiveBusiness",
	"image": "<?php echo ABC_THEME_URL; ?>/screenshot.png",
	"logo": "<?php echo ABC_THEME_URL; ?>/screenshot.png",
	"@id": "<?php echo home_url(); ?>",
	"name": "<?php echo bloginfo( 'name' ); ?>",
	"address": {
		"@type": "PostalAddress",
		"streetAddress": "Maskavas 418",
		"addressLocality": "Rīga",
		"addressCountry": "Latvija"
	},
	"geo": {
		"@type": "GeoCoordinates",
		"latitude": 56.895721,
		"longitude": 24.211000
	},
	"url": "<?php echo home_url(); ?>",
	"telephone": "28882332",
	"openingHoursSpecification": [
		{
		"@type": "OpeningHoursSpecification",
		"dayOfWeek": [
			"Monday",
			"Tuesday",
			"Wednesday",
			"Thursday",
			"Friday"
		],
		"opens": "09:00",
		"closes": "19:00"
		},
		{
		"@type": "OpeningHoursSpecification",
		"dayOfWeek": "Saturday",
		"opens": "10:00",
		"closes": "17:00"
		}
	]
	}
	</script>

	<?php wp_footer(); ?>
</body>
</html>
