<?php

/**
 * Styles
 */

function theme_styles() {
	wp_enqueue_style( 'normalize-css', ABC_THEME_URL . '/vendor/normalize-css/normalize.css' );
	wp_enqueue_style( 'googlefonts-css', 'https://fonts.googleapis.com/css?family=Montserrat:300,400,500,700&subset=latin-ext' );
	wp_enqueue_style( 'ionicicons-css', 'https://unpkg.com/ionicons@4.4.6/dist/css/ionicons.min.css' );
	wp_enqueue_style( 'application-css', ABC_THEME_URL . '/assets/styles/application.css' );

	if ( is_singular( 'cars' ) ) {
		wp_enqueue_style( 'lightslider-css', ABC_THEME_URL . '/vendor/lightslider/dist/css/lightslider.min.css' );
	}
}

/**
 * Scripts
 */

function theme_scripts() {
	wp_register_script( 'jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js', false, null, true );
	wp_register_script( 'nice-select-js', ABC_THEME_URL . '/vendor/jquery-nice-select/js/jquery.nice-select.min.js', false, null, true );
	wp_register_script( 'application-js', ABC_THEME_URL . '/assets/scripts/application.js', array( 'jquery' ), null, true );
	wp_register_script( 'map-js', ABC_THEME_URL . '/assets/scripts/map.js', false, null, true );
	wp_register_script( 'googlemaps-js', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyB3wtx38OGOysmg-qBp5BZPiC1_JqFOIPE&callback=initMap', false, null, true );
	// wp_register_script( 'ionicicons', 'https://unpkg.com/ionicons@4.2.2/dist/ionicons.js', false, null, true );
	wp_register_script( 'lightslider-js', ABC_THEME_URL . '/vendor/lightslider/dist/js/lightslider.min.js', false, null, true );
	wp_register_script( 'filter-sort-js', ABC_THEME_URL . '/assets/scripts/filter-sort.js', false, null, true );
	wp_register_script( 'lazyload-js', 'https://cdn.jsdelivr.net/npm/vanilla-lazyload@8.17.0/dist/lazyload.min.js', false, null, true );

	wp_enqueue_script( 'jquery' );
	wp_enqueue_script( 'nice-select-js' );
	// wp_enqueue_script( 'ionicicons' );

	if ( is_front_page() || 'templates/page--contact.tpl.php' === get_page_template_slug() ) {
		wp_enqueue_script( 'map-js' );
		wp_enqueue_script( 'googlemaps-js' );
	}

	if ( 'templates/inventory-items.tpl.php' === get_page_template_slug() ) {
		wp_enqueue_script( 'filter-sort-js' );
		wp_enqueue_script( 'lazyload-js' );
	}

	if ( is_singular( 'cars' ) ) {
		wp_enqueue_script( 'lightslider-js' );
	}

	wp_enqueue_script( 'application-js' );
}
