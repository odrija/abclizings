<?php

/**
 * Init ACF
 */

define( 'ACF_EARLY_ACCESS', '5' );
check_acf_plugin();

if ( function_exists( 'acf_add_options_page' ) ) {
	acf_add_options_page( array(
		'page_title' => 'Theme Settings',
		'menu_title' => 'Theme Settings',
	) );
	require_once dirname( __FILE__ ) . '/acf/acf-options.php';
	add_action( 'acf/init', 'options_custom_fields' );
}

require_once dirname( __FILE__ ) . '/acf/acf-landing-page.php';
add_action( 'acf/init', 'landing_page_custom_fields' );

require_once dirname( __FILE__ ) . '/acf/acf-inventory.php';
add_action( 'acf/init', 'inventory_custom_fields' );

require_once dirname( __FILE__ ) . '/acf/acf-cars.php';
add_action( 'acf/init', 'cars_custom_fields' );

require_once dirname( __FILE__ ) . '/acf/acf-contact-page.php';
add_action( 'acf/init', 'contact_page_custom_fields' );

require_once dirname( __FILE__ ) . '/acf/acf-enquiry-page.php';
add_action( 'acf/init', 'enquiry_page_custom_fields' );

function check_acf_plugin() {
	// Check if ACF is installed and active
	if ( is_admin() ) {
		if ( ! in_array( 'advanced-custom-fields/acf.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) &&  ! in_array( 'advanced-custom-fields-pro/acf.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) ) {
			function plugin_admin_notice__error() {
				$class   = 'notice notice-error';
				$message = __( 'Please install and activate Advanced Custom Fields plugin', 'abc' );

				printf( '<div class="%1$s"><p>%2$s</p></div>', $class, $message );
			}

			add_action( 'admin_notices', 'plugin_admin_notice__error' );
		}
	}
}
