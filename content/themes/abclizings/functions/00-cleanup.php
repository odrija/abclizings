<?php

/**
 * Remove WP Overhead
 */

function head_cleanup() {
	remove_action( 'wp_head', 'feed_links_extra', 3 ); // category feeds
	remove_action( 'wp_head', 'feed_links', 2 ); // post and comment feeds
	remove_action( 'wp_head', 'rsd_link' ); // EditURI link
	remove_action( 'wp_head', 'wlwmanifest_link' ); // windows live writer
	remove_action( 'wp_head', 'index_rel_link' ); // index link
	remove_action( 'wp_head', 'parent_post_rel_link', 10, 0 ); // previous link
	remove_action( 'wp_head', 'start_post_rel_link', 10, 0 ); // start link
	remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0 ); // links for adjacent posts
	remove_action( 'wp_head', 'wp_generator' ); // WP version
	remove_action( 'wp_head', 'print_emoji_detection_script', 7 ); //get rid of emojis
	remove_action( 'wp_print_styles', 'print_emoji_styles' ); //get rid of emojis
}

/**
 * Disable comments
 */

function disable_comments() {
	// Disable support for comments and trackbacks in post types
	function df_disable_comments_post_types_support() {
		$post_types = get_post_types();
		foreach ( $post_types as $post_type ) {
			if ( post_type_supports( $post_type, 'comments' ) ) {
				remove_post_type_support( $post_type, 'comments' );
				remove_post_type_support( $post_type, 'trackbacks' );
			}
		}
	}
	add_action( 'admin_init', 'df_disable_comments_post_types_support' );

	// Close comments on the front-end
	function df_disable_comments_status() {
		return false;
	}
	add_filter( 'comments_open', 'df_disable_comments_status', 20, 2 );
	add_filter( 'pings_open', 'df_disable_comments_status', 20, 2 );

	// Hide existing comments
	function df_disable_comments_hide_existing_comments( $comments ) {
		$comments = array();
		return $comments;
	}
	add_filter( 'comments_array', 'df_disable_comments_hide_existing_comments', 10, 2 );

	// Remove comments page in menu
	function df_disable_comments_admin_menu() {
		remove_menu_page( 'edit-comments.php' );
	}
	add_action( 'admin_menu', 'df_disable_comments_admin_menu' );

	// Redirect any user trying to access comments page
	function df_disable_comments_admin_menu_redirect() {
		global $pagenow;
		if ( $pagenow === 'edit-comments.php' ) {
			wp_redirect( admin_url() );
			exit;
		}
	}
	add_action( 'admin_init', 'df_disable_comments_admin_menu_redirect' );

	// Remove comments metabox from dashboard
	function df_disable_comments_dashboard() {
		remove_meta_box( 'dashboard_recent_comments', 'dashboard', 'normal' );
	}
	add_action( 'admin_init', 'df_disable_comments_dashboard' );

	// Remove comments links from admin bar
	function df_disable_comments_admin_bar() {
		if ( is_admin_bar_showing() ) {
			remove_action( 'admin_bar_menu', 'wp_admin_bar_comments_menu', 60 );
		}
	}
	add_action( 'init', 'df_disable_comments_admin_bar' );
}

// Cleanup text on paste in WYSIWYG
function configure_tinymce( $in ) {
	$in['paste_preprocess'] = "function(plugin, args){
    // Strip all HTML tags except those we have whitelisted
    var whitelist = 'p,span,b,strong,i,em,h3,h4,h5,h6,ul,li,ol';
    var stripped = jQuery('<div>' + args.content + '</div>');
    var els = stripped.find('*').not(whitelist);
    for (var i = els.length - 1; i >= 0; i--) {
      var e = els[i];
      jQuery(e).replaceWith(e.innerHTML);
    }
    // Strip all class and id attributes
    stripped.find('*').removeAttr('id').removeAttr('class');
    // Return the clean HTML
    args.content = stripped.html();
  }";
	return $in;
}
