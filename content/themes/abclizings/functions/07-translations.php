<?php

$translations = [
	'Vispārīgi'     => [
		'Izvēlne',
	],
	'Sākumlapa'     => [
		'Par mums',
		'Pakalpojumi',
		'Kontakti',
		'Adrese',
		'Darba laiks',
		'Pirmdiena – Piektdiena',
		'Sestdiena',
		'Svētdiena',
		'P – P',
		'S',
		'Sv',
	],
	'Auto katalogs' => [
		'Visas',
		'Visi',
		'Marka',
		'Virsbūves tips',
		'Transmisija',
		'Manuāla',
		'Automātiska',
		'Degvielas tips',
		'Piedāvājumā',
		'auto',
		'Pēc ievietošanas datuma',
		'Pēc cenas',
		'Pēc gada',
		'mēn.',
		'Par auto',
		'Tehniskie dati',
		'Dzinēja tilpums',
		'Pieteikties līzingam',
		'Pieteikums atvērsies jaunā logā',
		'Pārdots',
	],
];


function add_translation_strings( $groups = array() ) {
	foreach ( $groups as $group_name => $group ) {
		foreach ( $group as $string ) {
			$string = preg_replace( '/\s+/', ' ', $string );
			pll_register_string( $string, $string, $group_name );
		}
	}
}

add_translation_strings( $translations );

function _t( $string ) {
	$string = preg_replace( '/\s+/', ' ', $string );
	return pll__( $string );
}
