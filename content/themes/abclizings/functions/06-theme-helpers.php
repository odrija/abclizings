<?php

function count_words( $id ) {
	$post            = get_post( $id );
	$content         = $post->post_content;
	$decode_content  = html_entity_decode( $content );
	$strip_shortcode = strip_shortcodes( $decode_content );
	$strip_wp_tags   = wp_strip_all_tags( $strip_shortcode, true );
	$countwords      = str_word_count( $strip_wp_tags );
	return $countwords;
}

function strip_shortcode_gallery( $content ) {
	preg_match_all( '/' . get_shortcode_regex() . '/s', $content, $matches, PREG_SET_ORDER );

	if ( ! empty( $matches ) ) {
		foreach ( $matches as $shortcode ) {
			if ( 'gallery' === $shortcode[2] ) {
				$pos = strpos( $content, $shortcode[0] );
				if ( false !== $pos ) {
					return substr_replace( $content, '', $pos, strlen( $shortcode[0] ) );
				}
			}
		}
	}

	return $content;
}
