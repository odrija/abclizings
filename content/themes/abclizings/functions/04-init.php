<?php

function theme_init() {
	add_action( 'init', 'head_cleanup' );
	disable_comments();
	add_filter( 'tiny_mce_before_init', 'configure_tinymce' );
	add_theme_support( 'post-thumbnails' );

	add_image_size( 'inventory-thumb', 560, 360, array( 'center', 'center' ) );
	add_image_size( 'inventory-gallery-thumb', 300, 300, array( 'center', 'center' ) );
	add_image_size( 'inventory-large', 1200, 800, array( 'center', 'center' ) );

	add_action( 'wp_enqueue_scripts', 'theme_styles' );
	add_action( 'wp_enqueue_scripts', 'theme_scripts' );

	add_action( 'init', 'register_theme_menu' );

	load_theme_textdomain( 'abc', ABC_THEME_DIR . '/languages' );
}

add_action( 'after_setup_theme', 'theme_init' );

/**
 * Register Menu
 */

function register_theme_menu() {
	register_nav_menus(
		array(
			'header-menu' => __( 'Header Menu' ),
		)
	);
}

/**
 * Register custom URL parameters
 */

add_filter( 'query_vars', 'parameter_queryvars' );

function parameter_queryvars( $qvars ) {
	$qvars[] = 'car_id';
	return $qvars;
}
