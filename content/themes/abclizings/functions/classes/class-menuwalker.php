<?php

class Walker_Main_Menu extends Walker_Nav_menu {
	function start_lvl( &$output, $depth = 0, $args = array() ) {
		$indent  = str_repeat( "\t", $depth );
		$output .= "{$indent}";
	}

	function end_lvl( &$output, $depth = 0, $args = array() ) {
		$output .= "";
	}

	function start_el( &$output, $item, $depth = 0, $args = array(), $id = 0 ) {
		$output .= sprintf(
			"\n<a href='%s'%s>%s</a>\n",
			$item->url,
			( in_array( 'current-menu-item', $item->classes ) ) ? 'class="menu__item menu__item--active"' : 'class="menu__item"',
			$item->title
		);
	}
}
