<?php

class CarFilter {
	private static $post_type      = 'cars';
	private static $posts_per_page = -1;
	private static $all_cars;
	private static $cars_for_sale;

	/**
	 * Get all cars for sale and sold
	 */
	private static function get_all_cars() {
		$args = array(
			'post_type'  => static::$post_type,
			'meta_key'   => 'car_status',
			'meta_value' => 'for-sale',
		);

		$query_results = new WP_Query( $args );

		static::$cars_for_sale = $query_results;

		wp_reset_query();
	}

	/**
	 * Get all car query args
	 */
	private static function get_car_query_args() {
		$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
		$args  = array(
			'post_type'      => static::$post_type,
			'posts_per_page' => static::$posts_per_page,
			'paged'          => $paged,
			'orderby'        => array(
				'meta_value' => 'ASC',
				'date'       => 'DESC',
			),
			'meta_key'       => 'car_status',
		);

		return $args;
	}

	/**
	 * Get all cars for sale
	 */
	private static function get_cars_for_sale() {
		$query_results = new WP_Query( self::get_car_query_args() );

		static::$all_cars = $query_results;

		wp_reset_query();
	}

	/**
	 * Count all cars for sale
	 */
	private static function get_number_of_cars_for_sale() {
		return self::$cars_for_sale->found_posts;
	}

	/**
	 * Get all car makes
	 */
	private static function get_all_car_makes() {
		$all_car_makes = array();

		if ( self::$all_cars->have_posts() ) {
			while ( self::$all_cars->have_posts() ) {
				self::$all_cars->the_post();
				$make = get_field( 'car_data' )['car_make'];

				$all_car_makes[ sanitize_key( $make ) ] = $make;
			}
		}

		array_unique( $all_car_makes );
		asort( $all_car_makes );

		return $all_car_makes;
	}

	/**
	 * Get filter options
	 */
	private static function get_filter_options() {
		$filter_options = array();

		$filter_options['makes']        = self::get_all_car_makes();
		$filter_options['body']         = get_field_object( 'car_body' )['choices'];
		$filter_options['transmission'] = get_field_object( 'car_transmission' )['choices'];
		$filter_options['fuel']         = get_field_object( 'car_fuel' )['choices'];

		return $filter_options;
	}

	/**
	 * Show filter
	 */
	public static function show_filter() {
		$filter_options = self::get_filter_options();
		$ajax_url       = site_url() . '/wp-admin/admin-ajax.php';

		$html = "<form action='{$ajax_url}' method='POST' id='js-car-filter'>";
		// Make.
		$html .= '<select name="make" id="make">';
		$html .= '<option data-display="' . _t( 'Marka' ) . '" value="all">' . _t( 'Visas' ) . '</option>';
		foreach ( $filter_options['makes'] as $value => $label ) {
			$html .= "<option value='{$value}'>{$label}</option>";
		}
		$html .= '</select>';

		// Body type.
		$html .= '<select name="body" id="body">';
		$html .= '<option data-display=' . _t( 'Virsbūves tips' ) . '" value="all">' . _t( 'Visi' ) . '</option>';
		foreach ( $filter_options['body'] as $value => $label ) {
			$html .= "<option value='{$value}'>{$label}</option>";
		}
		$html .= '</select>';

		// Transmission.
		$html .= '<select name="transmission" id="transmission">';
		$html .= '<option data-display="' . _t( 'Transmisija' ) . '" value="all">' . _t( 'Visas' ) . '</option>';
		foreach ( $filter_options['transmission'] as $value => $label ) {
			$html .= "<option value='{$value}'>{$label}</option>";
		}
		$html .= '</select>';

		// Fuel.
		$html .= '<select name="fuel" id="fuel">';
		$html .= '<option data-display="' . _t( 'Degvielas tips' ) . '" value="all">' . _t( 'Visi' ) . '</option>';
		foreach ( $filter_options['fuel'] as $value => $label ) {
			$html .= "<option value='{$value}'>{$label}</option>";
		}
		$html .= '</select>';

		// Submit.
		$html .= '<button type="submit">';
		$html .= '<i class="icon ion-md-search"></i>';
		$html .= '</button>';
		$html .= '</form>';

		echo $html;
	}

	/**
	 * Show number of cars for sale
	 */
	public static function show_cars_for_sale_count() {
		$cars_for_sale = self::get_number_of_cars_for_sale();

		$html  = '<p>';
		$html .= _t( 'Piedāvājumā' );
		$html .= " <span>{$cars_for_sale}</span> ";
		$html .= _t( 'auto' );
		$html .= '</p>';

		echo $html;
	}

	/**
	 * Show sorting options
	 */
	public static function show_sorting() {
		$ajax_url = site_url() . '/wp-admin/admin-ajax.php';

		$html  = "<form action='{$ajax_url}' method='POST' id='js-car-sort'>";
		$html .= '<input type="hidden" id="filter-status" name="filter-status">';
		$html .= '<p class="text-right">';
		$html .= '<select name="sort" id="sort">';
		$html .= '<option value="date-desc">' . _t( 'Pēc ievietošanas datuma' ) . '</option>';
		$html .= '<option value="price-desc">' . _t( 'Pēc cenas' ) . ' ↓</option>';
		$html .= '<option value="price-asc">' . _t( 'Pēc cenas' ) . ' ↑</option>';
		$html .= '<option value="year-desc">' . _t( 'Pēc gada' ) . ' ↓</option>';
		$html .= '<option value="year-asc">' . _t( 'Pēc gada' ) . ' ↑</option>';
		$html .= '</select>';
		$html .= '</p>';
		$html .= '</form>';

		echo $html;
	}

	/**
	 * Show all cars
	 */

	public static function show_all_cars() {
		if ( self::$all_cars->have_posts() ) {
			while ( self::$all_cars->have_posts() ) {
				self::$all_cars->the_post();
				$car = new InventoryItem( get_the_ID() );
				echo $car->show_car_card();
			}
		}
	}

	/**
	 * Get sorted cars
	 */
	private static function get_sorted_cars() {
		if ( isset( $_POST['sort_type'] ) && ! empty( $_POST['sort_type'] ) ) {
			$addon  = false;
			$params = array();
			parse_str( $_POST['sort_type'], $params );

			if ( 'true' === $params['filter-status'] ) {
				if ( isset( $_POST['filter_query'] ) && ! empty( $_POST['filter_query'] ) ) {
					$filter_params = array();
					parse_str( $_POST['filter_query'], $filter_params );
					$addon = true;
					$args  = self::get_filtered_cars($filter_params);
				}
			} else {
				$addon = false;
				$args  = self::get_car_query_args();
			}

			$type = $params['sort'];

			switch ( $type) {
				case 'price-desc':
					$query_arr = array(
						'price_clause' => array(
							'key' => 'car_data_car_price',
							'compare' => 'EXISTS',
						),
						'status_clause' => array(
							'key' => 'car_status',
							'compare' => 'EXISTS'
						),
					);

					if ( $addon ) {
						$args['meta_query'][] = $query_arr;
					} else {
						$args['meta_query'] = array(
							'relation' => 'AND',
							$query_arr,
						);
					}

					$args['orderby'] = array(
						'status_clause' => 'ASC',
						'price_clause' => 'DESC',
					);

					break;

				case 'price-asc':
					$query_arr = array(
						'price_clause' => array(
							'key' => 'car_data_car_price',
							'compare' => 'EXISTS',
						),
						'status_clause' => array(
							'key' => 'car_status',
							'compare' => 'EXISTS'
						),
					);

					if ( $addon ) {
						$args['meta_query'][] = $query_arr;
					} else {
						$args['meta_query'] = array(
							'relation' => 'AND',
							$query_arr,
						);
					}

					$args['orderby'] = array(
						'status_clause' => 'ASC',
						'price_clause' => 'ASC',
					);

					break;

				case 'year-desc':
					$query_arr = array(
						'year_clause' => array(
							'key' => 'car_data_car_year',
							'compare' => 'EXISTS',
						),
						'status_clause' => array(
							'key' => 'car_status',
							'compare' => 'EXISTS'
						),
					);

					if ( $addon ) {
						$args['meta_query'][] = $query_arr;
					} else {
						$args['meta_query'] = array(
							'relation' => 'AND',
							$query_arr,
						);
					}

					$args['orderby'] = array(
						'status_clause' => 'ASC',
						'year_clause' => 'DESC',
					);

					break;

				case 'year-asc':
					$query_arr = array(
						'year_clause' => array(
							'key' => 'car_data_car_year',
							'compare' => 'EXISTS',
						),
						'status_clause' => array(
							'key' => 'car_status',
							'compare' => 'EXISTS'
						),
					);

					if ( $addon ) {
						$args['meta_query'][] = $query_arr;
					} else {
						$args['meta_query'] = array(
							'relation' => 'AND',
							$query_arr,
						);
					}

					$args['orderby'] = array(
						'status_clause' => 'ASC',
						'year_clause' => 'ASC',
					);

					break;

				default:
					$args['orderby']  = array(
						'meta_value' => 'ASC',
						'date'       => 'DESC',
					);
					$args['meta_key'] = 'car_status';

					break;
			}
		}

		return $args;
	}

	/**
	 * Show sorted cars
	 */
	public static function show_sorted_cars() {
		$args          = self::get_sorted_cars();
		$query_results = new WP_Query( $args );
		$output        = array();

		if ( $query_results->have_posts() ) {
			while ( $query_results->have_posts() ) {
				$query_results->the_post();
				$car = new InventoryItem( get_the_ID() );
				array_push( $output, $car->show_car_card() );
			}

			wp_send_json(
				array(
					'output' => $output,
				)
			);

			/* Restore original Post Data */
			wp_reset_postdata();
		} else {
			wp_send_json( array( 'output' => '<span class="message">Nav ierakstu!</span>' ) );
		}
	}

	/**
	 * Get filtered cars
	 */
	private static function get_filtered_cars( $params = false ) {
		$args = self::get_car_query_args();

		if ( $params && is_array( $params ) ) {
			// We're going to use our own parameters to filter cars
		} else if ( isset( $_POST['filter'] ) && ! empty( $_POST['filter'] ) ) {
			$params = array();
			parse_str( $_POST['filter'], $params );
		}

		if ( $params || ( isset( $_POST['filter'] ) && ! empty( $_POST['filter'] ) ) ) {
			$filtered_make         = ( 'all' === $params['make'] ) ? false : $params['make'];
			$filtered_body         = ( 'all' === $params['body'] ) ? false : $params['body'];
			$filtered_transmission = ( 'all' === $params['transmission'] ) ? false : $params['transmission'];
			$filtered_fuel         = ( 'all' === $params['fuel'] ) ? false : $params['fuel'];

			$args['meta_query'] = array(
				'relation' => 'AND',
			);

			if ( $filtered_make ) {
				$args['meta_query'][] = array(
					'key'     => 'car_data_car_make',
					'value'   => $filtered_make,
					'compare' => '=',
				);
			}

			if ( $filtered_body ) {
				$args['meta_query'][] = array(
					'key'     => 'car_data_car_body',
					'value'   => $filtered_body,
					'compare' => '=',
				);
			}

			if ( $filtered_transmission ) {
				$args['meta_query'][] = array(
					'key'     => 'car_data_car_transmission',
					'value'   => $filtered_transmission,
					'compare' => '=',
				);
			}

			if ( $filtered_fuel ) {
				$args['meta_query'][] = array(
					'key'     => 'car_data_car_fuel',
					'value'   => $filtered_fuel,
					'compare' => '=',
				);
			}
		}

		return $args;
	}

	/**
	 * Show filtered cars
	 */
	public static function show_filtered_cars() {
		$args          = self::get_filtered_cars();
		$query_results = new WP_Query( $args );
		$output        = array();

		if ( $query_results->have_posts() ) {
			while ( $query_results->have_posts() ) {
				$query_results->the_post();
				$car = new InventoryItem( get_the_ID() );
				array_push( $output, $car->show_car_card() );
			}

			wp_send_json(
				array(
					'output' => $output,
				)
			);

			/* Restore original Post Data */
			wp_reset_postdata();
		} else {
			wp_send_json( array( 'output' => '<span class="message">Nav ierakstu!</span>' ) );
		}	}

	public static function init() {
		self::get_all_cars();
		self::get_cars_for_sale();
	}
}
