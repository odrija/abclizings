<?php

class InventoryItem {

	public function __construct( $id ) {
		$this->id       = $id;
		$this->car_data = $this->get_car_data();

		$this->set_formatted_car_data();
	}

	public function show_title_bar() {
		$html  = '<div class="single-car__title-bar">';
		$html .= "<h1>{$this->car_data->make} {$this->car_data->model}</h1>";
		$html .= '<div class="single-car__title-bar__finance">';
		$html .= '<a href="' . home_url( '/lizings?car_id=' . $this->id ) . '" class="button" target="_blank" title="' . _t( 'Pieteikums atvērsies jaunā logā' ) . '">';
		$html .= _t( 'Pieteikties līzingam' );
		$html .= '</a>';
		$html .= '<div class="inventory__item__price">';
		$html .= $this->car_data->payment->formatted;
		$html .= '</div>';
		$html .= '</div>';
		$html .= '</div>';

		return $html;
	}

	private function get_car_data() {
		$car_data      = new stdClass();
		$car_data_list = get_field( 'car_data', $this->id );

		$car_data->url       = get_permalink( $this->id );
		$car_data->thumb     = wp_get_attachment_image_src( get_post_thumbnail_id( $this->id ), 'inventory-thumb' );
		$car_data->thumb_url = $car_data->thumb[0];
		$car_data->title     = get_the_title( $this->id );
		$car_data->make      = $car_data_list['car_make'];
		$car_data->model     = $car_data_list['car_model'];

		$car_data->price        = new StdClass();
		$car_data->price->plain = $car_data_list['car_price'];

		$car_data->payment        = new StdClass();
		$car_data->payment->plain = $car_data_list['car_payment'];

		$car_data->year = $car_data_list['car_year'];

		$car_data->mileage        = new StdClass();
		$car_data->mileage->plain = number_format( $car_data_list['car_mileage'], 0, ',', ' ' );

		$car_data->fuel        = new StdClass();
		$car_data->fuel->plain = $car_data_list['car_fuel'];

		$car_data->engine        = new StdClass();
		$car_data->engine->plain = $car_data_list['car_engine'];

		$car_data->transmission        = new StdClass();
		$car_data->transmission->plain = $car_data_list['car_transmission'];

		$car_data->body = $car_data_list['car_body'];

		$car_data->description = ( get_field( 'car_description', $this->id ) ) ? strip_tags( get_field( 'car_description', $this->id ), '<p><br><strong><em><a>' ) : '';

		$car_data->status = get_field( 'car_status' );

		return $car_data;
	}

	private function set_formatted_car_data() {
		$this->car_data->price->formatted        = $this->show_car_price();
		$this->car_data->payment->formatted      = $this->show_car_payment();
		$this->car_data->mileage->formatted      = $this->show_car_mileage();
		$this->car_data->fuel->formatted         = $this->show_car_fuel();
		$this->car_data->engine->formatted       = $this->show_car_engine();
		$this->car_data->transmission->formatted = $this->show_car_transmission();
	}

	private function get_fuel_label( $value ) {
		$fuel_labels = array(
			'petrol'     => _t( 'Benzīns' ),
			'diesel'     => _t( 'Dīzelis' ),
			'petrol-lpg' => _t( 'Benzīns / gāze' ),
		);

		return $fuel_labels[ $value ];
	}

	private function get_body_label( $value ) {
		$body_labels = array(
			'sedan'       => _t( 'Sedans' ),
			'hatch'       => _t( 'Hečbeks' ),
			'estate'      => _t( 'Universālis' ),
			'coupe'       => _t( 'Kupeja' ),
			'convertible' => _t( 'Kabriolets' ),
			'minivan'     => _t( 'Minivens' ),
			'suv'         => _t( 'Džips' ),
		);

		return $body_labels[ $value ];
	}

	private function get_transmission_label( $value ) {
		$transmission_labels = array(
			'manual'    => _t( 'Manuāla' ),
			'automatic' => _t( 'Automātika' ),
		);

		return $transmission_labels[ $value ];
	}

	private function show_car_price() {
		$html = "€ {$this->car_data->price->plain}";
		return $html;
	}

	private function show_car_payment() {
		$html  = "<span>€ {$this->car_data->payment->plain}</span>";
		$html .= '<span> / ' . _t( 'mēn.' ) . '</span>';
		return $html;
	}

	private function show_car_mileage() {
		$html = "{$this->car_data->mileage->plain} km";
		return $html;
	}

	private function show_car_fuel() {
		$html = $this->get_fuel_label( $this->car_data->fuel->plain );
		return $html;
	}

	private function show_car_engine() {
		$html = "{$this->car_data->engine->plain} cm<sup>3</sup>";
		return $html;
	}

	private function show_car_transmission() {
		$html = $this->get_transmission_label( $this->car_data->transmission->plain );
		return $html;
	}

	public function show_car_data() {
		$html  = '<article class="text-section single-car__details">';
		$html .= '<h2 class="text-section__title">';
		$html .= _t( 'Par auto' );
		$html .= '</h2>';
		$html .= '<p id="year">';
		$html .= '<i class="icon ion-md-calendar"></i>';
		$html .= "<span>{$this->car_data->year}</span>";
		$html .= '</p>';
		$html .= '<p id="mileage">';
		$html .= '<i class="icon ion-md-speedometer"></i>';
		$html .= "<span>{$this->car_data->mileage->formatted}</span>";
		$html .= '</p>';
		$html .= '<p id="price">';
		$html .= '<span>€</span>';
		$html .= "<span>{$this->car_data->price->plain}</span>";
		$html .= '</p>';
		$html .= '</article>';
		$html .= '<article class="text-section single-car__technical">';
		$html .= '<h2 class="text-section__title">';
		$html .= _t( 'Tehniskie dati' );
		$html .= '</h2>';
		$html .= '<p id="fuel">';
		$html .= '<span>';
		$html .= _t( 'Degvielas tips' );
		$html .= '</span>';
		$html .= "<span>{$this->car_data->fuel->formatted}</span>";
		$html .= '</p>';
		$html .= '<p id="engine-size">';
		$html .= '<span>';
		$html .= _t( 'Dzinēja tilpums' );
		$html .= '</span>';
		$html .= "<span>{$this->car_data->engine->formatted}</span>";
		$html .= '</p>';
		$html .= '<p id="transmission">';
		$html .= '<span>';
		$html .= _t( 'Transmisija' );
		$html .= '</span>';
		$html .= "<span>{$this->car_data->transmission->formatted}</span>";
		$html .= '</p>';
		$html .= '</article>';

		return $html;
	}

	public function show_car_gallery() {
		$gallery = get_field( 'car_gallery', $this->id );

		if ( !$gallery ) {
			$thumb_id = get_post_thumbnail_id( $this->id );

			$gallery[0]['sizes']['inventory-large']         = wp_get_attachment_image_src( $thumb_id, 'inventory-large' )[0];
			$gallery[0]['sizes']['inventory-gallery-thumb'] = wp_get_attachment_image_src( $thumb_id, 'inventory-gallery-thumb' )[0];
		}

		$html = '<div class="single-car__gallery" id="js-car-gallery">';

		foreach ( $gallery as $image ) {
			$image_url = $image['sizes']['inventory-large'];
			$thumb_url = $image['sizes']['inventory-gallery-thumb'];
			
			$html .= "<article class='single-car__gallery__item' data-thumb='{$thumb_url}'>";
			$html .= "<img src='{$image_url}' alt='{$this->car_data->title}'>";
			$html .= '</article>';
		}

		$html .= '</div>';

		return $html;
	}

	public function show_car_description() {
		$html  = '<div class="single-car__content">';
		$html .= $this->car_data->description;
		$html .= '</div>';
		return $html;
	}

	public function show_car_card() {
		if ( 'sold' === $this->car_data->status ) {
			$html  = "<div class='inventory__item inventory__item--sold'>";
		} else {
			$html  = "<a href='{$this->car_data->url}' title='{$this->car_data->title}' class='inventory__item'>";
		}

		$html .= "<div class='inventory__item__image lazy' data-bg='url(\"{$this->car_data->thumb_url}\")'></div>";
		$html .= '<div class="inventory__item__info">';
		$html .= ( 'sold' === $this->car_data->status ) ? '<span class="inventory__item__label">' . _t( 'Pārdots' ) . '</span>' : '';
		$html .= ( 'sold' === $this->car_data->status ) ? '<div class="inventory__item__overlay"></div>' : '';
		$html .= "<span class='inventory__item__title'>{$this->car_data->title}</span>";
		$html .= '<div class="inventory__item__data">';
		$html .= '<div class="inventory__item__price">';
		$html .= $this->car_data->payment->formatted;
		$html .= '</div>';
		$html .= '<div class="inventory__item__details">';
		$html .= "<span>{$this->car_data->year}</span>";
		$html .= "<span>{$this->car_data->mileage->formatted}</span>";
		$html .= "<span>{$this->car_data->price->formatted}</span>";
		$html .= '</div>';
		$html .= '</div>';
		$html .= '</div>';

		if ( 'sold' === $this->car_data->status ) {
			$html .= '</div>';
		} else {
			$html .= '</a>';
		}

		return $html;
	}
}
