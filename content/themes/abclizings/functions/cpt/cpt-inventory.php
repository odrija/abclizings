<?php

/**
 * Register Inventory item custom post type
 */

function create_inventory_cpt() {
	register_post_type(
		'cars',
		array(
			'labels'        => array(
				'name'          => __( 'Sludinājumi', 'abc' ),
				'singular_name' => __( 'Sludinājums', 'abc' ),
			),
			'public'        => true,
			'has_archive'   => false,
			'menu_icon'     => 'dashicons-screenoptions',
			'menu_position' => 2,
			'supports'      => array( 'title', 'thumbnail' ),
			'rewrite'       => array(
				'slug'       => 'auto-katalogs',
				'with_front' => false,
			),
		)
	);
}
