<?php

/**
 * Sort cars
 */
add_action( 'wp_ajax_sort_cars', 'sort_cars_function' );
add_action( 'wp_ajax_nopriv_sort_cars', 'sort_cars_function' );

function sort_cars_function() {
	CarFilter::show_sorted_cars();
	die();
}

/**
 * Filter cars
 */
add_action( 'wp_ajax_filter_cars', 'filter_cars_function' );
add_action( 'wp_ajax_nopriv_filter_cars', 'filter_cars_function' );

function filter_cars_function() {
	CarFilter::show_filtered_cars();
	die();
}
