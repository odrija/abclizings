<?php

function options_custom_fields() {
	acf_add_local_field_group(
		array(
			'key'      => 'group_options',
			'title'    => 'Theme Options',
			'fields'   => array(
				array(
					'key'      => 'landing_contacts_map',
					'label'    => 'SnazzyMaps embed URL',
					'name'     => 'landing_contacts_map',
					'type'     => 'text',
					'required' => 1,
				),
				array(
					'key'          => 'landing_contacts_address',
					'label'        => 'Adrese',
					'name'         => 'landing_contacts_address',
					'type'         => 'wysiwyg',
					'toolbar'      => 'typography',
					'media_upload' => 0,
					'required'     => 1,
				),
				array(
					'key'      => 'landing_contacts_workdays',
					'label'    => 'Darba laiks, pirmdiena – piektdiena',
					'name'     => 'landing_contacts_workdays',
					'type'     => 'text',
					'required' => 1,
				),
				array(
					'key'      => 'landing_contacts_saturdays',
					'label'    => 'Darba laiks, sestdiena',
					'name'     => 'landing_contacts_saturdays',
					'type'     => 'text',
					'required' => 1,
				),
				array(
					'key'      => 'landing_contacts_sundays',
					'label'    => 'Darba laiks, svētdiena',
					'name'     => 'landing_contacts_sundays',
					'type'     => 'text',
					'required' => 1,
				),
				array(
					'key'      => 'contact_facebook',
					'label'    => 'Facebook URL',
					'name'     => 'contact_facebook',
					'type'     => 'text',
					'required' => 0,
				),
				array(
					'key'      => 'contact_instagram',
					'label'    => 'Instagram URL',
					'name'     => 'contact_instagram',
					'type'     => 'text',
					'required' => 0,
				),
			),
			'location' => array(
				array(
					array(
						'param'    => 'options_page',
						'operator' => '==',
						'value'    => 'acf-options-theme-settings',
					),
				),
			),
		)
	);
};

