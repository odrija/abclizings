<?php

function contact_page_custom_fields() {
	acf_add_local_field_group(
		array(
			'key'            => 'group_contact_page',
			'title'          => 'Kontaktu lapa',
			'fields'         => array(
				array(
					'key'     => 'contact_message',
					'name'    => 'contact_message',
					'type'    => 'message',
					'message' => 'Adresi un darba laiku var rediģēt sadaļā “Theme options”',
				),
				array(
					'key'          => 'contact_blocks',
					'label'        => 'Teksta bloki',
					'name'         => 'contact_blocks',
					'type'         => 'repeater',
					'required'     => 0,
					'button_label' => 'Pievienot teksta bloku',
					'collapsed'    => 'contact_blocks_header',
					'layout'       => 'row',
					'sub_fields'   => array(
						array(
							'key'      => 'contact_blocks_header',
							'label'    => 'Teksta bloka virsraksts',
							'name'     => 'contact_blocks_header',
							'type'     => 'text',
							'required' => 1,
						),
						array(
							'key'          => 'contact_blocks_text',
							'label'        => 'Teksta bloka teksts',
							'name'         => 'contact_blocks_text',
							'type'         => 'wysiwyg',
							'media_upload' => 0,
							'toolbar'      => 'minimal',
							'required'     => 1,
						),
					),
				),
			),
			'location'       => array(
				array(
					array(
						'param'    => 'page_template',
						'operator' => '==',
						'value'    => 'templates/page--contact.tpl.php',
					),
				),
			),
			'position'       => 'acf_after_title',
			'hide_on_screen' => array(
				0 => 'the_content',
			),
		)
	);
}
