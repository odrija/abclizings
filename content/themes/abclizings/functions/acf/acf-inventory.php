<?php

function inventory_custom_fields() {
	acf_add_local_field_group(
		array(
			'key'            => 'group_inventory',
			'title'          => 'Auto katalogs',
			'fields'         => array(
				array(
					'key'      => 'inventory_heading',
					'label'    => 'Virsraksts',
					'name'     => 'inventory_heading',
					'type'     => 'textarea',
					'required' => 1,
				),
				array(
					'key'      => 'inventory_subheading',
					'label'    => 'Apakšvirsraksts',
					'name'     => 'inventory_subheading',
					'type'     => 'textarea',
					'required' => 1,
				),
			),
			'location'       => array(
				array(
					array(
						'param'    => 'page_template',
						'operator' => '==',
						'value'    => 'templates/inventory-items.tpl.php',
					),
				),
			),
			'position'       => 'acf_after_title',
			'hide_on_screen' => array(
				0 => 'the_content',
			),
		)
	);
}
