<?php

function enquiry_page_custom_fields() {
	acf_add_local_field_group(
		array(
			'key'            => 'group_enquiry_page',
			'title'          => 'Līzinga pieteikuma lapa',
			'fields'         => array(
				array(
					'key'      => 'enquiry_subheader',
					'name'     => 'enquiry_subheader',
					'label'    => 'Apakšvirsraksts',
					'type'     => 'text',
					'required' => 1,
				),
				array(
					'key'      => 'enquiry_description',
					'name'     => 'enquiry_description',
					'label'    => 'Apraksts',
					'type'     => 'textarea',
					'required' => 0,
				),
				array(
					'key'      => 'enquiry_image',
					'name'     => 'enquiry_image',
					'label'    => 'Attēls',
					'type'     => 'image',
					'required' => 0,
				),
				array(
					'key'       => 'enquiry_form',
					'name'      => 'enquiry_form',
					'label'     => 'Pieteikuma forma',
					'type'      => 'post_object',
					'post_type' => 'wpcf7_contact_form',
					'required'  => 1,
				),
			),
			'location'       => array(
				array(
					array(
						'param'    => 'page_template',
						'operator' => '==',
						'value'    => 'templates/page--enquiry.tpl.php',
					),
				),
			),
			'position'       => 'acf_after_title',
			'hide_on_screen' => array(
				0 => 'the_content',
			),
		)
	);
}

