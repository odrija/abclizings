<?php

function cars_custom_fields() {
	acf_add_local_field_group(
		array(
			'key'            => 'group_inventory_item',
			'title'          => 'Sludinājums',
			'fields'         => array(
				array(
					'key'        => 'car_data',
					'label'      => 'Par auto',
					'name'       => 'car_data',
					'type'       => 'group',
					'layout'     => 'row',
					'sub_fields' => array(
						array(
							'key'      => 'car_make',
							'label'    => 'Auto marka',
							'name'     => 'car_make',
							'type'     => 'text',
							'required' => 1,
						),
						array(
							'key'      => 'car_model',
							'label'    => 'Auto modelis',
							'name'     => 'car_model',
							'type'     => 'text',
							'required' => 1,
						),
						array(
							'key'      => 'car_price',
							'label'    => 'Pilna cena',
							'name'     => 'car_price',
							'type'     => 'number',
							'prepend'  => '€',
							'step'     => 1,
							'required' => 1,
						),
						array(
							'key'      => 'car_payment',
							'label'    => 'Mēneša maksājums',
							'name'     => 'car_payment',
							'type'     => 'number',
							'prepend'  => '€',
							'step'     => 1,
							'required' => 1,
						),
						array(
							'key'      => 'car_year',
							'label'    => 'Ražošanas gads',
							'name'     => 'car_year',
							'type'     => 'number',
							'step'     => 1,
							'required' => 1,
						),
						array(
							'key'      => 'car_mileage',
							'label'    => 'Odometra rādītājs',
							'name'     => 'car_mileage',
							'type'     => 'number',
							'append'   => 'km',
							'step'     => 1,
							'required' => 1,
						),
						array(
							'key'      => 'car_fuel',
							'label'    => 'Degvielas tips',
							'name'     => 'car_fuel',
							'type'     => 'radio',
							'layout'   => 'horizontal',
							'choices'  => array(
								'petrol'     => _t( 'Benzīns' ),
								'diesel'     => _t( 'Dīzelis' ),
								'petrol-lpg' => _t( 'Benzīns / gāze' ),
							),
							'required' => 1,
						),
						array(
							'key'      => 'car_engine',
							'label'    => 'Dzinēja tilpums',
							'name'     => 'car_engine',
							'type'     => 'number',
							'append'   => 'cm³',
							'step'     => 1,
							'required' => 1,
						),
						array(
							'key'      => 'car_transmission',
							'label'    => 'Transmisija',
							'name'     => 'car_transmission',
							'type'     => 'radio',
							'choices'  => array(
								'manual'    => _t( 'Manuāla' ),
								'automatic' => _t( 'Automātiska' ),
							),
							'layout'   => 'horizontal',
							'required' => 1,
						),
						array(
							'key'      => 'car_body',
							'label'    => 'Virsbūves tips',
							'name'     => 'car_body',
							'type'     => 'select',
							'choices'  => array(
								'sedan'       => _t( 'Sedans' ),
								'hatch'       => _t( 'Hečbeks' ),
								'estate'      => _t( 'Universālis' ),
								'coupe'       => _t( 'Kupeja' ),
								'convertible' => _t( 'Kabriolets' ),
								'minivan'     => _t( 'Minivens' ),
								'suv'         => _t( 'Džips' ),
							),
							'required' => 1,
						),
					),
				),
				array(
					'key'          => 'car_description',
					'label'        => 'Auto apraksts',
					'name'         => 'car_description',
					'type'         => 'wysiwyg',
					'tabs'         => 'visual',
					'toolbar'      => 'minimal',
					'media_upload' => 0,
					'required'     => 0,
				),
				array(
					'key'      => 'car_gallery',
					'label'    => 'Galerija',
					'name'     => 'car_gallery',
					'type'     => 'gallery',
					'required' => 0,
				),
			),
			'location'       => array(
				array(
					array(
						'param'    => 'post_type',
						'operator' => '==',
						'value'    => 'cars',
					),
				),
			),
			'position'       => 'acf_after_title',
			'hide_on_screen' => array(
				0 => 'the_content',
			),
		)
	);

	acf_add_local_field_group(
		array(
			'key'      => 'group_inventory_item_side',
			'title'    => 'Sludinājums',
			'fields'   => array(
				array(
					'key'      => 'car_status',
					'name'     => 'car_status',
					'label'    => 'Sludinājuma statuss',
					'type'     => 'radio',
					'choices'  => array(
						'for-sale' => 'Pieejams',
						'sold'     => 'Pārdots',
					),
					'required' => 1,
				),
			),
			'location' => array(
				array(
					array(
						'param'    => 'post_type',
						'operator' => '==',
						'value'    => 'cars',
					),
				),
			),
			'position' => 'side',
		)
	);
};
