<?php

function landing_page_custom_fields() {
	acf_add_local_field_group(
		array(
			'key'            => 'group_landing_page',
			'title'          => 'Sākumlapa',
			'fields'         => array(
				array(
					'key'   => 'landing_hero',
					'label' => 'Lapas augšdaļa',
					'name'  => 'landing_hero',
					'type'  => 'accordion',
				),
				array(
					'key'      => 'landing_page_heading',
					'label'    => 'Virsraksts',
					'name'     => 'landing_page_heading',
					'type'     => 'text',
					'required' => 1,
				),
				array(
					'key'           => 'landing_page_button',
					'label'         => 'Galvenās darbības saite',
					'name'          => 'landing_page_button',
					'type'          => 'link',
					'return_format' => 'array',
					'required'      => 1,
				),
				array(
					'key'   => 'landing_about',
					'label' => 'Par mums',
					'name'  => 'landing_about',
					'type'  => 'accordion',
				),
				array(
					'key'          => 'landing_about_blocks',
					'label'        => 'Teksta bloki',
					'name'         => 'landing_about_blocks',
					'type'         => 'repeater',
					'required'     => 0,
					'button_label' => 'Pievienot teksta bloku',
					'collapsed'    => 'landing_about_blocks_header',
					'layout'       => 'table',
					'sub_fields'   => array(
						array(
							'key'      => 'landing_about_blocks_header',
							'label'    => 'Teksta bloka virsraksts',
							'name'     => 'landing_about_blocks_header',
							'type'     => 'text',
							'required' => 1,
						),
						array(
							'key'      => 'landing_about_blocks_image',
							'label'    => 'Teksta bloka attēls',
							'name'     => 'landing_about_blocks_image',
							'type'     => 'image',
							'required' => 0,
						),
						array(
							'key'          => 'landing_about_blocks_text',
							'label'        => 'Teksta bloka teksts',
							'name'         => 'landing_about_blocks_text',
							'type'         => 'wysiwyg',
							'media_upload' => 0,
							'toolbar'      => 'minimal',
							'required'     => 1,
						),
					),
				),
				array(
					'key'   => 'landing_services',
					'label' => 'Pakalpojumi',
					'name'  => 'landing_services',
					'type'  => 'accordion',
				),
				array(
					'key'          => 'landing_services_blocks',
					'label'        => 'Teksta bloki',
					'name'         => 'landing_services_blocks',
					'type'         => 'repeater',
					'required'     => 0,
					'button_label' => 'Pievienot teksta bloku',
					'collapsed'    => 'landing_services_blocks_header',
					'layout'       => 'table',
					'sub_fields'   => array(
						array(
							'key'      => 'landing_services_blocks_header',
							'label'    => 'Teksta bloka virsraksts',
							'name'     => 'landing_services_blocks_header',
							'type'     => 'text',
							'required' => 1,
						),
						array(
							'key'          => 'landing_services_blocks_text',
							'label'        => 'Teksta bloka teksts',
							'name'         => 'landing_services_blocks_text',
							'type'         => 'wysiwyg',
							'media_upload' => 0,
							'toolbar'      => 'minimal',
							'required'     => 1,
						),
					),
				),
				array(
					'key'   => 'landing_contacts',
					'label' => 'Kontakti',
					'name'  => 'landing_contacts',
					'type'  => 'accordion',
				),
				array(
					'key'          => 'landing_contacts_info',
					'label'        => 'Kontakti',
					'name'         => 'landing_contacts_info',
					'type'         => 'wysiwyg',
					'toolbar'      => 'typography',
					'media_upload' => 0,
					'required'     => 1,
					'instructions' => 'Pārējā kontaktinformācija rediģējama pie tēmas uzstādījumiem (Theme Settings)',
				),
			),
			'location'       => array(
				array(
					array(
						'param'    => 'page_template',
						'operator' => '==',
						'value'    => 'templates/page--landing.tpl.php',
					),
				),
			),
			'position'       => 'acf_after_title',
			'hide_on_screen' => array(
				0 => 'the_content',
			),
		)
	);
}


add_filter( 'acf/fields/wysiwyg/toolbars', 'my_toolbars' );
function my_toolbars( $toolbars ) {
	// Uncomment to view format of $toolbars
	/*
	echo '< pre >';
		print_r($toolbars);
	echo '< /pre >';
	die;
	*/

	$toolbars['Typography']    = array();
	$toolbars['Typography'][1] = array( 'formatselect', 'bold', 'italic', 'link', 'unlink', 'undo', 'redo' );

	$toolbars['Minimal']    = array();
	$toolbars['Minimal'][1] = array( 'bold', 'italic', 'link', 'unlink', 'bullist', 'numlist', 'undo', 'redo' );

	$toolbars['Empty']    = array();
	$toolbars['Empty'][1] = array( 'fullscreen' );

	return $toolbars;
}
