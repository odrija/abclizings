<?php

add_filter( 'manage_posts_columns', 'custom_columns' );
function custom_columns( $columns ) {
	$columns = array(
		'cb'             => '<input type="checkbox" />',
		'featured_image' => 'Attēls',
		'title'          => 'Nosaukums',
		'year'           => 'Gads',
		'price'          => 'Cena',
		'comments'       => '<span class="vers"><div title="Comments" class="comment-grey-bubble"></div></span>',
		'date'           => 'Date',
	);
	return $columns;
}

add_action( 'manage_posts_custom_column', 'custom_columns_data', 10, 2 );
function custom_columns_data( $column, $post_id ) {
	switch ( $column ) {
		case 'featured_image':
			the_post_thumbnail( 'thumbnail' );
			break;
		case 'year':
			$car_data = get_field( 'car_data' );
			echo $car_data['car_year'];
			break;
		case 'price':
			$car_data = get_field( 'car_data' );
			echo '€ ' . $car_data['car_price'];
			break;
	}
}
